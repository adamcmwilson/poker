"Mark43 Poker" Judger
=====================

## Project Description

The Poker application is a Scala script.

To run it, you will need to have Python (for the test runner), the Java JRE, and Scala for the application.

To determine if your system is setup to run the app and test runner, run the following commands and verify you have similar output:

```
$ python
Python 3.6.3 (default, Oct 24 2017, 14:48:20) 
```
```
$ java -version
openjdk version "1.8.0_144"
```
```
$ scala

Welcome to Scala 2.12.4-20171023-143547-unknown (OpenJDK 64-Bit Server VM, Java 1.8.0_144).
```

Instructions on installing Scala:
http://www.scala-lang.org/download/
 

## Test Runner Instructions:

Copies of the included test-runner scripts have been placed in `/src`

Use the following command to invoke the Poker application from the test runner

```
$ ./run_tests "scala Poker.scala"
     // or
$ ./run_tests3 "scala Poker.scala"
```

The provided test input files (01 &amp; 02) have been included in `src/tests`.
I have also added additional tests to verify other game outcomes.
