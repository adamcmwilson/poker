import scala.annotation.switch
import scala.io._

object Poker {

  def main(args: Array[String]): Unit = {

    val playerCount: Int = StdIn.readInt()

    require(playerCount > 0, "You need some players to play...")
    require(playerCount > 0, "You need more than one player to play...")

    val handsInPlay = readAllHands(playerCount)

    require(handsInPlay.length == playerCount,
      s"There are $playerCount players, but only ${handsInPlay.length} hands played.")

    displayWinner(getWinner(handsInPlay.toArray))
  }

  private def readAllHands(playerCount: Int) = {
    for {
      playerId <- 1 to playerCount
    } yield readSingleHand(playerId)
  }

  def readSingleHand(playerId: Int): Hand = {
    new Hand(StdIn.readLine())
  }

  def getWinner(hands: Array[Hand]): Array[String] = {
    val winner = hands.sorted.last
    return hands.filter(_.compare(winner) == 0).map(_.playerId)
  }

  def displayWinner(winners: Array[String]): Unit = {
    println(s"${winners.mkString(" ")}")
  }

}

class Card(shortHand: String) extends Ordered[Card] {

  require(shortHand != null)
  require(!shortHand.isEmpty)

  val suit: Char = shortHand.charAt(1)

  // Ensure valid Suit value:
  require(Array[Char]('h', 'd', 's', 'c')
    .contains(suit), s"$suit is an invalid suit")

  private val rankInput: Char = shortHand.charAt(0)

  // Ensure valid Rank value:
  require(Array[Char]('2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A')
    .contains(rankInput), s"$rankInput is an invalid Rank"
  )

  val rank: Int =
    (rankInput: @switch) match {
      case '2' => 2
      case '3' => 3
      case '4' => 4
      case '5' => 5
      case '6' => 6
      case '7' => 7
      case '8' => 8
      case '9' => 9
      case 'T' => 10
      case 'J' => 11
      case 'Q' => 12
      case 'K' => 13
      case 'A' => 14 // <-- A can be high or low in some situations
    }

  override def toString = s"$suit$rankInput"

  override def compare(that: Card): Int = {
    if (this.rank > that.rank) return 1
    if (this.rank < that.rank) return -1
    return 0
  }
}

class Hand(handEntry: String) extends Ordered[Hand] {

  val StraightFlush = 6
  val ThreeOfAKind = 5
  val Straight = 4
  val Flush = 3
  val Pair = 2
  val HighCard = 1

  require(handEntry.contains(" "), s"Invalid Hand entry [$handEntry]")

  private val handEntryElements = handEntry.split(" ")

  val playerId = handEntryElements(0)

  val cards = for (entry <- handEntryElements.slice(1, handEntryElements.length))
    yield new Card(entry)

  private val cardRanks = cards.map(card => card.rank).sorted.reverse

  /**
    * Determine the score of this Hand
    */
  val score: Int = {
    if (isStraightFlush()) StraightFlush
    else if (isThreeOfAKind()) ThreeOfAKind
    else if (isStraight()) Straight
    else if (isFlush()) Flush
    else if (isPair()) Pair
    else HighCard
  }

  val tieBreakerRank: Int = {
    if (score == Pair) {
      getHighPairRank()
    } else {
      getHighCardRank()
    }
  }

  val highCardTieBreakerRank: Int = {
    val nextHighestCards = cardRanks.filterNot(_ == cardRanks.max)
    if (nextHighestCards.isEmpty) cardRanks.max else nextHighestCards.max
  }

  private def isStraightFlush(): Boolean = isFlush() && isStraight()

  private def isThreeOfAKind(): Boolean = {
    for (rank <- cardRanks) {
      if (cardRanks.containsSlice(Seq(rank, rank, rank))) return true
    }

    return false
  }

  private def isPair(): Boolean = {
    for (rank <- cardRanks) {
      if (cardRanks.containsSlice(Seq(rank, rank))) return true
    }

    return false
  }

  private def isStraight(): Boolean = {

    // A's can be hi or low:
    if (cardRanks.max == 14 // Ace as '1'
      && cardRanks.contains(2)
      && cardRanks.contains(3)) return true

    (cardRanks.min to cardRanks.max).length == cards.length
  }

  private def isFlush(): Boolean = {
    val suit = cards(0).suit
    cards.filter(card => !card.suit.equals(suit)).length == 0
  }

  private def getHighCardRank(): Int = cardRanks.max

  private def getHighPairRank(): Int = {
    for (rank <- cardRanks) {
      if (cardRanks.containsSlice(Seq(rank, rank))) return rank
    }

    return 0
  }

  override def toString = s"Hand:[$handEntry] | Score: $score | Tie Breaker Rank: $tieBreakerRank"

  override def compare(that: Hand): Int = {

    /*
     return -1  => this < that
     return  0  => considered same
     return  1  => this > that
   */

    // Consider initial score:
    if (this.score > that.score) return 1
    if (this.score < that.score) return -1

    // When 3-of-a-kind the rank of the set decides the winner:
    if (this.isThreeOfAKind()) {
      if (this.getHighCardRank() > that.getHighCardRank()) return 1
      else if (this.getHighCardRank() < that.getHighCardRank()) return -1
      else return 0 // <- required to compare against itself or copy
    }

    // When comparing pairs the winner is chosen by
    //   1) the rank of the pair
    //   2) the rank of the next highest card
    if (this.isPair()) {
      val thisHighPairRank = this.getHighPairRank()
      val thatHighPairRank = that.getHighPairRank()

      if (thisHighPairRank > thatHighPairRank) return 1
      if (thisHighPairRank < thatHighPairRank) return -1

      val thisOtherRank = this.cardRanks.sorted.filterNot(_ == thisHighPairRank).apply(0)
      val thatOtherRank = that.cardRanks.sorted.filterNot(_ == thatHighPairRank).apply(0)

      if (thisOtherRank > thatOtherRank) return 1
      if (thisOtherRank < thatOtherRank) return -1
      return 0
    }

    // High-card:
    this.cardRanks.sorted.reverse.zip(that.cardRanks.sorted.reverse)
      .foreach(cardPair => {
        if (cardPair._1 > cardPair._2) return 1
        else if (cardPair._1 < cardPair._2) return -1
      })

    return 0
  }

}
